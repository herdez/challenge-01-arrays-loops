# Challenge 1

## Solving Problems with Arrays & Loops


#### Steps
- Clone repo.

#### Deriverables
- Upload your github/gitlab repository url to Trello.

#### Final result

Our client has decided that it is important to use functions to solve the problem and he has
given us some tests to prove our functions. The statements/declarations in the
console.assert(…) describe the expected output from the function when provided with a given
input and should evaluate to true if you have written the function correctly.

> You are not to use helper methods like `.split()`, `substr()`, `.reverse()`. Client has decided
to manage iteration manually to solve problems.
